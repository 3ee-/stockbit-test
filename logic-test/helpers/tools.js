function cleanString(str) {
  return str.replace(/[^\w]/g).toLowerCase().split("").sort().join();
}

function groupAnagrams(anagram) {
  let res = {};
  for (let word of anagram) {
    let clean = cleanString(word);
    if (res[clean]) {
      res[clean].push(word);
    } else {
      res[clean] = [word];
    }
  }
  return Object.values(res);
}

module.exports = {
  groupAnagrams,
};
