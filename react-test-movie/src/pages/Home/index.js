import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import styled from "styled-components";

// TODO: import dispatcher
import { startfetchAllFilms, clearAllFilms } from "../../modules/films/actions";

// TODO: import components
import {
  Loading,
  Wrapper,
  Container,
  CardWrapper,
  Col,
  Row,
  Card,
  Input,
  Button,
  Text,
  ImagePopUp,
} from "../../components";

function Home() {
  // TODO: State
  const [search, setSearch] = useState(localStorage.getItem("search") || null);
  const [page, setPage] = useState(1);
  const [selectedImage, setSelectedImage] = useState("");

  // TODO: Ref
  const loader = useRef(null);

  // TODO: Reducers
  const isLoading = useSelector((state) => state.films.isLoading);
  const films = useSelector((state) => state.films.films);

  // TODO: Dispatcher
  const dispatch = useDispatch();

  // TODO: Actions
  const searchSubmit = () => {
    dispatch(clearAllFilms());
    dispatch(startfetchAllFilms(search, page));
    localStorage.setItem("search", search);
  };

  const handleObserver = (entities) => {
    const target = entities[0];
    if (target.isIntersecting) {
      setPage((page) => page + 1);
    }
  };

  // TODO: Lifecycle
  useEffect(() => {
    if (films.length > 9) {
      var options = {
        root: null,
        rootMargin: "20px",
        threshold: 1.0,
      };

      const observer = new IntersectionObserver(handleObserver, options);
      if (loader.current) {
        observer.observe(loader.current);
      }
    }
  }, [films]);

  useEffect(() => {
    if (page > 1) {
      dispatch(startfetchAllFilms(search, page));
    }
  }, [page]);

  return (
    <Wrapper>
      {isLoading && <Loading />}
      {selectedImage && (
        <ImagePopUp
          source={selectedImage}
          closeEvent={() => setSelectedImage("")}
        />
      )}
      <Container>
        <InputRow>
          <Input value={search} onChange={(e) => setSearch(e.target.value)} />
          <Button onClick={searchSubmit}>Search</Button>
        </InputRow>
        <CardWrapper>
          {films &&
            films.map((item) => (
              <Card key={item.imdbID}>
                <Row>
                  <Image
                    onClick={() => setSelectedImage(item.Poster)}
                    src={item.Poster}
                  />
                  <Col>
                    <Link to={`/${item.imdbID}`}>
                      <Text>{item.Title}</Text>
                      <Text>{item.Year}</Text>
                      <Text>{item.Type}</Text>
                    </Link>
                  </Col>
                </Row>
              </Card>
            ))}
        </CardWrapper>
        <div ref={loader}></div>
      </Container>
    </Wrapper>
  );
}

// TODO: Style here

const InputRow = styled.div`
  display: flex;
  flex-direction: row;
  position: sticky;
  top: 0px;
`;

const Image = styled.img`
  width: 90px;
  height: 130px;
  margin-right: 20px;
  object-fit: cover;
`;

export default Home;
