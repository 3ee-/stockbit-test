// TODO: import components
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import styled from "styled-components";
import {
  Loading,
  Wrapper,
  Container,
  CardWrapper,
  Col,
  Row,
  Text,
} from "../../components";

// TODO: import dispatcher
import { startfetchDetailFilms } from "../../modules/films/actions";

function Detail(props) {
  // TODO: Params
  let { id } = useParams();

  // TODO: Reducers
  const isLoading = useSelector((state) => state.films.isLoading);
  const detailFilm = useSelector((state) => state.films.detailFilm);

  // TODO: Dispatcher
  const dispatch = useDispatch();

  // TODO: Lifecycle
  useEffect(() => {
    dispatch(startfetchDetailFilms(id));
  }, []);

  return (
    <Wrapper>
      {isLoading && <Loading />}
      <Container>
        <Row>
          <Image src={detailFilm && detailFilm.Poster} />
          <Col>
            <Title>{detailFilm && detailFilm.Title}</Title>
            <Text>{detailFilm && detailFilm.Genre}</Text>
            <Text>{detailFilm && detailFilm.Rated}</Text>
            <Text>{detailFilm && detailFilm.Director}</Text>
            <Text>{detailFilm && detailFilm.Country}</Text>
            <Text>{detailFilm && detailFilm.Actors}</Text>
            <Text>{detailFilm && detailFilm.Released}</Text>
            <Text>{detailFilm && detailFilm.Type}</Text>
            <Text>{detailFilm && detailFilm.Year}</Text>
            <Text>{detailFilm && detailFilm.imdbVotes} Total Votes</Text>
            <Text>{detailFilm && detailFilm.imdbRating} Rating</Text>
          </Col>
        </Row>
        <LinkText>
          <Link to="/">Back to list</Link>
        </LinkText>
      </Container>
    </Wrapper>
  );
}

const Image = styled.img`
  width: 200px;
  height: 300px;
  margin-right: 20px;
  object-fit: cover;
`;

const Title = styled.p`
  font-size: 18px;
  margin: 0;
  font-weight: 900;
  margin-bottom: 8px;
  text-transform: capitalize;
  color: #323232;
  text-decoration: none;
`;

const LinkText = styled.p`
  font-size: 18px;
  margin: 0;
  margin-bottom: 8px;
  text-transform: capitalize;
  color: #323232 !important;
  text-decoration: underline;
  text-align: center;
  margin-top: 30px;
  cursor: pointer;
`;

export default Detail;
