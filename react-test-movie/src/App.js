import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";

// TODO: Import components
import Home from "./pages/Home";
import Detail from "./pages/Detail";

// TODO: Import store
import configureStore from "./configStore";

const store = configureStore();

function App() {
  return (
    <div className="container">
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/:id">
              <Detail />
            </Route>
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
