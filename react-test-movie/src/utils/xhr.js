import axios from "axios";

const defaultHeaders = {
  "Content-Type": "application/json",
};

const axiosBase = (params) =>
  axios.create({
    baseURL: `${process.env.REACT_APP_BASE_URL}?apikey=faf7e5bb${params}`,
    timeout: 30000,
    headers: defaultHeaders,
  });

export default axiosBase;
