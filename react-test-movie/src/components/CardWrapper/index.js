import styled from "styled-components";

const CardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 40px;
`;

export { CardWrapper };
