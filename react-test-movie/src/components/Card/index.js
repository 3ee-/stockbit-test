import styled from "styled-components";

const Card = styled.div`
  width: 100%;
  height: auto;
  margin-bottom: 20px;
  cursor: pointer;
`;

export { Card };
