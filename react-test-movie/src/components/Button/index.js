import styled from "styled-components";

const Button = styled.button`
  min-height: 40px;
  font-size: 16px;
  background: #fe718d;
  color: #fff;
  padding: 6px 16px;
  border: none;
  border-bottom: 1px solid lightgray;

  &:focus {
    outline: none;
    border-bottom: 2px solid gray !important;
  }
`;

export { Button };
