import styled from "styled-components";

const Input = styled.input`
  width: 100%;
  height: 40px;
  font-size: 16px;
  background: #f4f4f4;
  border: none;
  border-bottom: 1px solid lightgray;

  &:focus {
    outline: none;
    border-bottom: 2px solid gray !important;
  }
`;

export { Input };
