import styled from "styled-components";

const Wrapper = styled.div`
  width: 100%;
  background-color: #f4f4f4;
  height: auto;
  min-height: 100vh;
`;

export { Wrapper };
