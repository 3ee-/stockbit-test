import styled from "styled-components";

const Text = styled.p`
  font-size: 14px;
  margin: 0;
  margin-bottom: 8px;
  text-transform: capitalize;
  color: #323232;
  text-decoration: none;
`;

export { Text };
