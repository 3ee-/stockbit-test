import styled from "styled-components";

const Col = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export { Col };
