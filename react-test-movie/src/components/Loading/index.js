import styled from "styled-components";

// TODO: Image gif
import { Spinner } from "../../assets/img";

function Loading() {
  return (
    <Wrapper>
      <Image src={Spinner} />
      <Title>Still Loading...</Title>
    </Wrapper>
  );
}

// TODO: Style here
const Wrapper = styled.div`
  width: 100%;
  background-color: rgba(0, 0, 0, 0.3);
  position: fixed;
  top: 0;
  height: 100%;
  overflow-y: hidden;
  display: flex;
  flex: 1;
  z-index: 10;
  align-items: center;
  justify-content: center;
`;

const Title = styled.p`
  font-size: 16px;
  text-align: center;
  color: #fff;
  margin-top: 8px;
  margin-bottom: 0;
`;

const Image = styled.img`
  width: 100px;
`;

export { Loading };
