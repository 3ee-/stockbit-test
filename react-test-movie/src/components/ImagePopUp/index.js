import styled from "styled-components";

function ImagePopUp({ source, closeEvent }) {
  return (
    <Wrapper>
      <Image src={source} />
      <Close onClick={closeEvent}>x</Close>
    </Wrapper>
  );
}

// TODO: Style here
const Wrapper = styled.div`
  width: 100%;
  background-color: rgba(0, 0, 0, 0.7);
  position: fixed;
  top: 0;
  height: 100%;
  overflow-y: hidden;
  display: flex;
  flex: 1;
  z-index: 10;
  align-items: center;
  justify-content: center;
`;

const Image = styled.img`
  width: 300px;
  height: 500px;
`;

const Close = styled.p`
  color: #fff;
  text-transform: lowercase;
  position: absolute;
  right: 50px;
  top: 10px;
  font-size: 32px;
  cursor: pointer;
`;

export { ImagePopUp };
