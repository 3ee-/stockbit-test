import styled from "styled-components";

const Container = styled.div`
  padding: 0 100px;
  width: 50%;
  margin: auto;
  padding-top: 50px;
`;

export { Container };
