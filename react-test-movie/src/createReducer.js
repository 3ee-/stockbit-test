import { combineReducers } from "redux";

import filmReducer from "./modules/films/reducer";

const rootReducer = combineReducers({
  films: filmReducer,
});

export default rootReducer;
