const PREFIX = "modules/films";

export const SET_LOADING = `${PREFIX}/setLoading`;
export const START_FETCH_ALL_FILMS = `${PREFIX}/startfetchAllFilms`;
export const FINISH_FETCH_ALL_FILMS = `${PREFIX}/finishfetchAllFilms`;

export const START_FETCH_DETAIL_FILMS = `${PREFIX}/startfetchDetailFilms`;
export const FINISH_FETCH_DETAIL_FILMS = `${PREFIX}/finishfetchDetailFilms`;

export const CLEAR_ALL_FILMS = `${PREFIX}/clearAllFilms`;
