import * as actionTypes from "./actionTypes";

const initialState = {
  isLoading: false,
  films: [],
  filmsTotal: 0,
  detailFilm: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_LOADING:
      return {
        ...state,
        isLoading: action.result,
      };
    case actionTypes.FINISH_FETCH_ALL_FILMS:
      return {
        ...state,
        films: [...state.films, ...action.result.Search],
        filmsTotal: action.result.totalResults || 0,
      };
    case actionTypes.CLEAR_ALL_FILMS:
      return {
        ...state,
        films: [],
        filmsTotal: 0,
      };
    case actionTypes.FINISH_FETCH_DETAIL_FILMS:
      return {
        ...state,
        detailFilm: action.result,
      };
    default:
      return state;
  }
}
