import * as actionTypes from "./actionTypes";

export function setLoading(result) {
  return {
    type: actionTypes.SET_LOADING,
    result,
  };
}

export function startfetchAllFilms(search, page = 1) {
  return {
    type: actionTypes.START_FETCH_ALL_FILMS,
    search,
    page,
  };
}

export function finishfetchAllFilms(result) {
  return {
    type: actionTypes.FINISH_FETCH_ALL_FILMS,
    result,
  };
}

export function startfetchDetailFilms(id) {
  return {
    type: actionTypes.START_FETCH_DETAIL_FILMS,
    id,
  };
}

export function finishfetchDetailFilms(result) {
  return {
    type: actionTypes.FINISH_FETCH_DETAIL_FILMS,
    result,
  };
}

export function clearAllFilms() {
  return {
    type: actionTypes.CLEAR_ALL_FILMS,
  };
}
