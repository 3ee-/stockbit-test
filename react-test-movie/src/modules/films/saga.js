import { put, takeLatest, call, all } from "redux-saga/effects";

import * as actionTypes from "./actionTypes";
import * as actions from "./actions";

// TODO: Import base axios
import xhr from "../../utils/xhr";

function* fetchFilmsSaga({ search, page }) {
  try {
    yield put(actions.setLoading(true));
    const { data } = yield call(xhr(`&s=${search}&page=${page}`).get);
    yield put(actions.finishfetchAllFilms(data));
  } catch (error) {
    console.log(error.message);
  } finally {
    yield put(actions.setLoading(false));
  }
}

function* fetchDetailFilmSaga({ id }) {
  try {
    yield put(actions.setLoading(true));
    const { data } = yield call(xhr(`&i=${id}`).get);
    yield put(actions.finishfetchDetailFilms(data));
  } catch (error) {
    console.log(error.message);
  } finally {
    yield put(actions.setLoading(false));
  }
}

export default function* watchBrands() {
  yield all([takeLatest(actionTypes.START_FETCH_ALL_FILMS, fetchFilmsSaga)]);
  yield all([
    takeLatest(actionTypes.START_FETCH_DETAIL_FILMS, fetchDetailFilmSaga),
  ]);
}
