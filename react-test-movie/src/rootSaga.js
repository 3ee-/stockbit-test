import { all, fork } from "redux-saga/effects";

import watchFilms from "./modules/films/saga";

export default function* rootSaga() {
  yield all([fork(watchFilms)]);
}
